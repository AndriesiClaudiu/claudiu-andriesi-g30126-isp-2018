package g30126.andriesi.claudiu.l2.e1;

import java.util.Scanner;

public class Number {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Read first number:");
		int a = keyboard.nextInt();
		System.out.println("Read second number:");
		int b = keyboard.nextInt();
		System.out.println("Min(" + a + ", " + b + ") = " + Math.max(a, b));
		keyboard.close();
	}
}
