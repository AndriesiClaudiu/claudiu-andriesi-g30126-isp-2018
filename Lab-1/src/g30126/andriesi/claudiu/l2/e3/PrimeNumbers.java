package g30126.andriesi.claudiu.l2.e3;

import java.util.Scanner;

public class PrimeNumbers {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Read first number: ");
		int numberA = keyboard.nextInt();
		System.out.println("Read second number: ");
		int numberB = keyboard.nextInt();
		showPrimeNumbersBetween(numberA, numberB);
		keyboard.close();
	}

	private static boolean isPrime(int number) {
		if (number <= 1)
			return false;
		int n = number / 2;
		for (int i = 2; i <= n; i++) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	private static void showPrimeNumbersBetween(int a, int b) {
		int contor = 0;
		if (a > b)
			System.out.println("Wrong interval!");
		else {
			System.out.println("Prime numbers: ");
			for (int i = a; i <= b; i++) {
				if (isPrime(i)){
					System.out.print(i + " ");
					contor++;
				}
			}
			System.out.println("\nThere are " + contor + " prime numbers between " + a + " and " + b);
		}
	}
}
