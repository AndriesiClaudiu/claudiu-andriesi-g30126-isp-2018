package g30126.andriesi.claudiu.l2.e4;

import java.util.Scanner;
import java.util.Arrays;

public class MaxElement {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Read length: ");
		int N = keyboard.nextInt();
		int[] array = new int[N];
		System.out.println("Read array: ");
		for (int i = 0; i < N; i++) {
			System.out.print("array[" + i + "]= ");
			array[i] = keyboard.nextInt();
		}
		System.out.println("Array: ");
		for (int i = 0; i < N; i++)
			System.out.print(array[i] + " ");
		getMax(array, N);
		keyboard.close();
	}

	private static void getMax(int array[], int n) {
		int max = array[0];
		
		for (int i = 1; i < n; i++) {
			if (array[i] > max)
				max = array[i];
		}
		System.out.println("\nMax from array is: " + max);
	}
}
