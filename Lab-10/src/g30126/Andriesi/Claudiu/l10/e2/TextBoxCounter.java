package g30126.Andriesi.Claudiu.l10.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TextBoxCounter extends JFrame {

    private int number = 0;
    private Button button;
    private JLabel textField;
    TextBoxCounter(){
        setSize(200,200);
        init();
        setVisible(true);
    }
    public void init(){
        setLayout(null);
        button = new Button("+");
        button.setBounds(75, 100, 50,50);
        textField = new JLabel(number + "");
        textField.setBounds(0,0,200,100);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                number++;
                textField.setText(number + "");
            }
        });
        add(textField);
        add(button);

    }

    public static void main(String[] args) {
        new TextBoxCounter();
    }
}
