package g30126.Andriesi.Claudiu.l10.e3;

import java.awt.Button;
import java.awt.TextField;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.*;

public class Files extends JFrame{
    private Button load;
    private TextField fileNameField;
    private TextArea textArea;
    Files(){
        setSize(700,1000);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }
    public void init(){
        setLayout(null);
        load = new Button("Load");
        fileNameField = new TextField();
        textArea = new TextArea();
        fileNameField.setBounds(200, 10,250,50);
        load.setBounds(460,15,70,40);
        textArea.setBounds(0,100,700,850);
        add(fileNameField);
        add(load);
        add(textArea);
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = new File(fileNameField.getText());
                if (file.exists()){
                    try{
                        FileInputStream fis = new FileInputStream(file);
                        while(fis.available() > 0){
                            textArea.append((char)fis.read() + "");
                        }
                        fis.close();
                    }catch (IOException e3){
                        System.out.println(e3.getStackTrace());
                    }


                }
                else
                    textArea.setText("File not found!");
            }
        });

    }
}

class Main{
    public static void main(String[] args) {
        new Files();
    }
}