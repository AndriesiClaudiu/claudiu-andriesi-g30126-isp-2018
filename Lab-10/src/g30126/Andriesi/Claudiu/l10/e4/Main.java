package g30126.Andriesi.Claudiu.l10.e4;

import javax.naming.ldap.Control;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        Model model = new Model();
        System.out.println(model.toString());
        Controller controller = new Controller(view, model);
    }
}
