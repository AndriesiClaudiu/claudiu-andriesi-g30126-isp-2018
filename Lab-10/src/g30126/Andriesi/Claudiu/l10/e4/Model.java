package g30126.Andriesi.Claudiu.l10.e4;

public class Model {
    private int[][] matrix = new int[3][3];
    Model(){
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++)
                matrix[i][j] = -1;
        }
    }

    public void setCell(int i, int j, int number){
        if (matrix[i][j] == -1)
            matrix[i][j] = number;
    }

    public int getCellValue(int i, int j){
        return matrix[i][j];
    }

    public int[][] getMatrix(){
        return matrix;
    }

    public String toString(){
        String string = "";
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++) {
                string += Integer.toString(matrix[i][j]);
                if (j == 2)
                    string += "\n";
            }
        }
        return string;
    }
}
