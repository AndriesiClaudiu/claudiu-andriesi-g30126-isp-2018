package g30126.Andriesi.Claudiu.l10.e4;

import com.sun.xml.internal.messaging.saaj.soap.JpegDataContentHandler;

import javax.swing.*;
import java.awt.*;

public class View extends JFrame{

    private JPanel gamePanel;
    private JLabel score;
    private JPanel title;
    private JButton[][] buttons = new JButton[3][3];
    View(){
        setSize(600,730);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
        setVisible(true);
    }

    public void init(){
        setLayout(null);
        gamePanel = new JPanel();
        gamePanel.setBounds(0,100, 600,630);
        gamePanel.setBackground(Color.darkGray);
        gamePanel.setLayout(null);
        title = new JPanel();
        title.setBounds(0,0,600,100);
        title.setBackground(Color.darkGray);
        score = new JLabel("Tic Tac Toe");
        score.setFont(new Font("Roboto",Font.BOLD, 50));
        score.setForeground(Color.white);
        title.add(score);
        add(title);
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                buttons[i][j] = new JButton("");
                buttons[i][j].setBounds(i*190 + 5*(i+1),j*190 + 5*(j+1), 180,180);
                buttons[i][j].setFont(new Font("Roboto", Font.BOLD, 70));
                gamePanel.add(buttons[i][j]);

            }
        }
        add(gamePanel);
    }

    public JButton getButton(int i, int j){
        return buttons[i][j];
    }

    public JButton[][] getButtons() {
        return buttons;
    }
}
