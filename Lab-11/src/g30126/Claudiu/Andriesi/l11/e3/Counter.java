package g30126.Claudiu.Andriesi.l11.e3;

public class Counter extends Thread {

    private int min;
    private int max;
    Counter(String name, int min, int max){
        super(name);
        this.min = min;
        this.max = max;
    }

    Counter(String name){
        super(name);
        this.min = 0;
        this.max = 100;
    }

    public void run(){
        for(int i=min;i<=max;i++){
            System.out.println(getName() + " i = "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}