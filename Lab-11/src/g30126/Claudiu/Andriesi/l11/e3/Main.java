package g30126.Claudiu.Andriesi.l11.e3;

public class Main {
    public static void main(String[] args) {
        Counter c1 = new Counter("counter1");
        Counter c2 = new Counter("counter2", 101, 200);

        try {
            c1.start();
            c1.join();
            c2.start();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
