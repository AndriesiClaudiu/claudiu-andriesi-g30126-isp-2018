package g30126.Claudiu.Andriesi.l11.e4;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Board board = new Board(15);
        board.start();
        Robot.setLimit(15);
        for (int i = 0; i < 20; i++)
            board.addRobot(new Robot(i));


        board.join();

    }
}
