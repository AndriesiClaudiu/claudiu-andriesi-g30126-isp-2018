package g30126.Claudiu.Andriesi.l11.e6;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Button extends JButton {


    private boolean isPressed = false;

    //Constructor
    public Button(String name) {
        super(name);
        setPreferredSize(new Dimension(85, 60));
        setHorizontalTextPosition(SwingConstants.CENTER);
        setVerticalAlignment(SwingConstants.CENTER);
        setBorder(null);
        isPressed = false;
        setFocusable(false);
        setForeground(Color.white);
        setBackground(Color.darkGray);
        setFont(new Font("Baron", Font.BOLD, 25));
        setDefaultCapable(false);
        super.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                setBackground(Color.decode("#BDBEBD"));
                if (!isPressed)
                    isPressed = !isPressed;

            }

            @Override
            public void mousePressed(MouseEvent e) {
                setBackground(Color.lightGray);
                setBorder(new LineBorder(Color.decode("#BDBEBD"), 3, false));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                setBackground(Color.decode("#E2E2E2"));
                setBorder(new LineBorder(Color.decode("#EEEEEE"), 3, false));
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(Color.decode("#E2E2E2"));
                setBorder(null);
                setForeground(Color.darkGray);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Color.darkGray);
                setBorder(null);
                setForeground(Color.white);

            }
        });
    }

    public boolean isPressed() {
        return isPressed;
    }

    public void negate() {
        this.isPressed = !this.isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }
}
