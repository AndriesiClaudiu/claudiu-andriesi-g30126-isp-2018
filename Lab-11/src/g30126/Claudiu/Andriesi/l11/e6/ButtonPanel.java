package g30126.Claudiu.Andriesi.l11.e6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonPanel extends JPanel {

    private Button start;
    private Button reset;
    private Button stop;

    //Constructor
    public ButtonPanel() {
        super();
        setSize(300, 110);
        setBackground(Color.darkGray);
        start = new Button("Start");
        reset = new Button("Reset");
        stop = new Button("Stop");
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!start.isPressed()) {
                    start.setPressed(true);
                    reset.setPressed(false);
                }

            }
        });
        reset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!reset.isPressed() && start.isPressed()) {
                    reset.setPressed(true);
                    start.negate();
                }

            }
        });

        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start.setPressed(false);
                reset.setPressed(false);
            }
        });
        add(start);
        add(stop);
        add(reset);
    }

    public Button getStart() {
        return start;
    }

    public Button getReset() {
        return reset;
    }

    public void setStart(Button start) {
        this.start = start;
    }

    public void setReset(Button reset) {
        this.reset = reset;
    }

    public void setStop(Button stop) {
        this.stop = stop;
    }

    public Button getStop() {

        return stop;
    }
}
