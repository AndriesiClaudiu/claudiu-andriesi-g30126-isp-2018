package g30126.Claudiu.Andriesi.l11.e6;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    private TextPanel textPanel;
    private ButtonPanel buttonPanel;
    //Constructor
    public Frame(){
        super("StopWatch");

        setSize(300,300);
        setResizable(false);
        setLocation(1000,250);
        setBackground(Color.LIGHT_GRAY);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        ImageIcon imageIcon = new ImageIcon("F:\\IntelliJ\\Projects\\Time\\im.jpg");
        setIconImage(imageIcon.getImage());


        JPanel panel = new JPanel();
        panel.setBackground(Color.white);
        panel.setLayout(new FlowLayout(SwingConstants.VERTICAL));
        panel.setSize(new Dimension(300,300));
        add(panel);

        textPanel = new TextPanel();
        buttonPanel = new ButtonPanel();

        panel.add(textPanel);
        panel.add(buttonPanel);
        panel.setVisible(true);
        setVisible(true);
    }

    public TextPanel getTextPanel() {
        return textPanel;
    }

    public ButtonPanel getButtonPanel() {
        return buttonPanel;
    }
}
