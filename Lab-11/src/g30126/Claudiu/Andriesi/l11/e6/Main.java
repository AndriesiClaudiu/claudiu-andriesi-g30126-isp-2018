package g30126.Claudiu.Andriesi.l11.e6;

import java.io.FileReader;

public class Main {
    public static void main(String[] args) {
        Frame frame = new Frame();
        TicTac ticTac = new TicTac();
        TicTacSecunde ticTacSecunde = new TicTacSecunde();
        Minute minute = new Minute(ticTac);
        Secunde secunde = new Secunde(ticTac, ticTacSecunde);
        Milisecunde milisecunde = new Milisecunde(frame, ticTac, ticTacSecunde, minute, secunde);
        minute.start();
        secunde.start();
        milisecunde.start();
    }
}

