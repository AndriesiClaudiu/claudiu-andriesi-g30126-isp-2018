package g30126.Claudiu.Andriesi.l11.e6;


public class Milisecunde extends Thread {
    private Frame frame;
    private TicTac ticTac;
    private Minute minute;
    private Secunde secunde;
    private int milisecunde = 0;
    private TicTacSecunde ticTacSecunde;

    //Constructor
    public Milisecunde(Frame frame, TicTac ticTac, TicTacSecunde ticTacSecunde, Minute minute, Secunde secunde) {
        this.frame = frame;
        this.ticTac = ticTac;
        this.minute = minute;
        this.ticTacSecunde = ticTacSecunde;
        this.secunde = secunde;
    }

    public void run() {
        while (true) {
            if (frame.getButtonPanel().getStart().isPressed() && !frame.getButtonPanel().getReset().isPressed()) {
                try {
                    sleep(1);
                } catch (InterruptedException e) {
                    System.err.println("ERROR in milisecunde");
                }
                if (milisecunde == 999) {
                    milisecunde = 0;
                    ticTacSecunde.tic();
                } else
                    milisecunde++;
            } else if (!frame.getButtonPanel().getStart().isPressed() && !frame.getButtonPanel().getReset().isPressed()) {
            } else if (frame.getButtonPanel().getReset().isPressed() && !frame.getButtonPanel().getStart().isPressed()) {
                milisecunde = 0;
                minute.setMinute(0);
                secunde.setSecunde(0);
            }
            frame.getTextPanel().getTextField().setText(String.format("%02d", minute.getMinute()) + ":" + String.format("%02d", secunde.getSecunde()) + ":" + String.format("%02d", milisecunde/10));
        }
    }
}
