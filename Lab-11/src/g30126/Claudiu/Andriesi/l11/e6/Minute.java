package g30126.Claudiu.Andriesi.l11.e6;



public class Minute extends Thread {
    private TicTac ticTac;
    private int minute = 0;

    //Constructor
    public Minute( TicTac ticTac) {
        this.ticTac = ticTac;
    }

    public void run() {
        while (true) {
            ticTac.waitForTic();
            if (minute == 59) {
                minute = 0;
            } else
                minute++;
        }
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}
