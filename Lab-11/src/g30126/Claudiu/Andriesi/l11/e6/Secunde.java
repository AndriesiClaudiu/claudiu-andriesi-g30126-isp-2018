package g30126.Claudiu.Andriesi.l11.e6;



public class Secunde extends Thread {
    private TicTac ticTac;
    private int secunde = 0;
    private TicTacSecunde ticTacSecunde;


    //Constructor
    public Secunde(TicTac ticTac, TicTacSecunde ticTacSecunde) {
        this.ticTacSecunde = ticTacSecunde;
        this.ticTac = ticTac;
    }

    public void run() {
        while (true) {
            ticTacSecunde.waitForTic();
            if (secunde == 59) {
                ticTac.tic();
                secunde = 0;
            } else
                secunde++;
        }
    }

    public void setSecunde(int secunde) {
        this.secunde = secunde;
    }

    public int getSecunde() {
        return secunde;
    }
}
