package g30126.Claudiu.Andriesi.l11.e6;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class TextField extends JTextField{

    //Constructor
    public TextField(String content){
        super(content);
        setPreferredSize(new Dimension(280,160));
        setEditable(false);
        setHighlighter(null);
        setForeground(Color.white);
        setBackground(Color.darkGray);
        setBorder(new LineBorder(Color.BLACK));
        setHorizontalAlignment(CENTER);
        setFont(new Font("Calibri", Font.BOLD, 75));
    }
}
