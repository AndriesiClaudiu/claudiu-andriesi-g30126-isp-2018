package g30126.Claudiu.Andriesi.l11.e6;

import javax.swing.*;
import java.awt.*;

public class TextPanel extends JPanel {

    private TextField textField;
    //Constructor
    public TextPanel(){
        super();
        setSize(300,180);
        setBackground(Color.white);
        textField = new TextField("00:00:00");
        add(textField);
    }

    public TextField getTextField() {
        return textField;
    }
}
