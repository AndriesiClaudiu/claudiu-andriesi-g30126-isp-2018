package g30126.Claudiu.Andriesi.l11.e6;

public class TicTac {
    private boolean tic = false;

    public synchronized void waitForTic() {
        while (!tic) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("ERROR in waiting");
            }
        }
        tic = false;
    }

    public synchronized void tic() {
        tic = true;
        notify();
    }
}
