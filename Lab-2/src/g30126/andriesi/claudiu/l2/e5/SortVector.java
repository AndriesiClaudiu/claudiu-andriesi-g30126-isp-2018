package g30126.andriesi.claudiu.l2.e5;

import java.util.Random;

public class SortVector {
	public static void main(String[] args) {
		Random random = new Random();
		int[] vector = new int[10];
		System.out.print("Array:\n");
		for (int i = 0; i < 10; i++) {
			vector[i] = random.nextInt(100);
			System.out.print(vector[i] + " ");
		}
		BubbleSort(vector);
		System.out.println("\nSorted array:");
		for (int i = 0; i < 10; i++) {
			System.out.print(vector[i] + " ");
		}
	}

	private static void BubbleSort(int[] arr) {
		int n = arr.length;
		int temp = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 1; j < (n - i); j++) {
				if (arr[j - 1] > arr[j]) {
					temp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = temp;
				}

			}
		}
	}

}
