package g30126.andriesi.claudiu.l2.e6;

import java.util.Random;

public class Factorial {
	public static void main(String[] args) {
		Random random = new Random();
		int n;
		do {
			n = random.nextInt(5);
		} while (n < 0);
		System.out.println("Nonrecursive Fact:");
		int factNonRec = nonRecursiveFact(n);
		System.out.println("fact(" + n + ")= " + factNonRec);
		System.out.println("Recursive Fact:");
		int factorialRec = recursiveFact(n);
		System.out.println("fact(" + n + ")= " + factorialRec);

	}

	private static int nonRecursiveFact(int n) {
		int factorial = 1;
		while (n > 0)
			factorial *= n--;
		return factorial;
	}

	private static int recursiveFact(int n) {
		if (n == 0 || n == 1)
			return 1;
		return n * recursiveFact(n - 1);

	}
}
