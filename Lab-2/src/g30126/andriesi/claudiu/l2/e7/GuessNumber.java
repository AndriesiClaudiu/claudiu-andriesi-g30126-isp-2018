package g30126.andriesi.claudiu.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
	public static void main(String[] args) {
		Random random = new Random();
		Scanner keyboard = new Scanner(System.in);
		int n = random.nextInt(10);
		System.out.println(n);
		int i = 3;
		while (i-- > 0) {
			System.out.println("Guess the number: ");
			int numberA = keyboard.nextInt();
			if (n == numberA) {
				System.out.println("It's right!");
				System.exit(0);
			} else if (n < numberA) {
				System.out.println("Wrong answer, your number it too high");
			} else
				System.out.println("Wrong answer, your number it too low");
			System.out.println(i + " left");
		}
		System.out.println("Game over!");
		keyboard.close();
	}
}
