import g30126.Andriesi.Claudiu.l3.e6.MyPoint;
import org.junit.*;

public class TestMyPoint extends Assert {

    private MyPoint myPoint = new MyPoint();

    @Test
    public void testSetX() {
        myPoint.setX(1);
        assertEquals(1, myPoint.getX());
    }

    @Test
    public void testSetY() {
        myPoint.setY(1);
        assertEquals(1, myPoint.getY());
    }

    @Test
    public void testSetXY() {
        myPoint.setXY(1, 1);
        assertEquals(1, myPoint.getX());
        assertEquals(1, myPoint.getX());
    }

    @Test
    public void testDistance(){
        myPoint.setXY(0,0);
        assertEquals(1,(int)myPoint.distance(1,0));
    }

    @Test
    public void testDistance2(){
        myPoint.setXY(0,0);
        assertEquals(1,(int)myPoint.distance(new MyPoint(1,0)));
    }
}
