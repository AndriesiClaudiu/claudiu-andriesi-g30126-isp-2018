package g30126.Andriesi.Claudiu.l3.UnpairedNumber;

public class Main {

    public static int solution(int[] A) {
        int result = 0;
        for(int i:A){
            result ^=i; 
        }
        return result;
    }

    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);

        if (result == 7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
}
