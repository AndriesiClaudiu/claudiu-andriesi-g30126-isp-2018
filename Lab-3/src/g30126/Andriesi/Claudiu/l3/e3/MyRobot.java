package g30126.Andriesi.Claudiu.l3.e3;

import becker.robots.*;

public class MyRobot
{
    public static void main(String[] args)
    {
        // Set up the initial situation
        City neamt = new City();
        Thing field = new Thing(neamt, 1, 2);
        Robot rob = new Robot(neamt, 1, 1, Direction.NORTH);
        for (int i = 0; i < 5; i++)
            rob.move();
        rob.turnLeft();
        rob.turnLeft();
        for (int i = 0; i < 5; i++)
            rob.move();

    }
}