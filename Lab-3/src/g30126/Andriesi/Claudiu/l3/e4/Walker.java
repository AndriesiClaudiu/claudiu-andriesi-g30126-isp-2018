package g30126.Andriesi.Claudiu.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;

public class Walker {
    public static void main(String[] args) {
        City ny = new City();
        Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
        Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
        Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
        Wall blockAve3 = new Wall(ny, 2, 2, Direction.SOUTH);
        Wall blockAve4 = new Wall(ny, 1, 2, Direction.EAST);
        Wall blockAve5 = new Wall(ny, 2, 2, Direction.EAST);
        Wall blockAve6 = new Wall(ny, 1, 1, Direction.NORTH);
        Wall blockAve7 = new Wall(ny, 1, 2, Direction.NORTH);


        Robot one = new Robot(ny, 0, 2, Direction.WEST);
        one.move();
        one.move();
        one.turnLeft();
        one.move();
        one.move();
        one.move();
        one.turnLeft();
        one.move();
        one.move();
        one.move();
        one.turnLeft();
        one.move();
        one.move();
        one.move();
        one.turnLeft();
        one.move();





    }


}
