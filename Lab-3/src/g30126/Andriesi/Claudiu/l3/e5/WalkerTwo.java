package g30126.Andriesi.Claudiu.l3.e5;

import becker.robots.*;


public class WalkerTwo {
    public static void main(String[] args) {


        City Cluj = new City();
        Thing newspaper = new Thing(Cluj, 2, 2);
        Robot robo = new Robot(Cluj, 1, 2, Direction.SOUTH);
        Wall blockAve0 = new Wall(Cluj, 1, 2, Direction.NORTH);
        Wall blockAve1 = new Wall(Cluj, 1, 1, Direction.NORTH);
        Wall blockAve2 = new Wall(Cluj, 1, 2, Direction.EAST);
        Wall blockAve3 = new Wall(Cluj, 1, 2, Direction.SOUTH);
        Wall blockAve4 = new Wall(Cluj, 1, 1, Direction.WEST);
        Wall blockAve5 = new Wall(Cluj, 2, 1, Direction.WEST);
        Wall blockAve6 = new Wall(Cluj, 2, 1, Direction.SOUTH);
        robo.turnLeft();
        robo.turnLeft();
        robo.turnLeft();
        robo.move();
        robo.turnLeft();
        robo.move();
        robo.turnLeft();
        robo.move();
        robo.pickThing();

        //The robot picked the newspaper

        robo.turnLeft();
        robo.turnLeft();
        robo.move();
        robo.turnLeft();
        robo.turnLeft();
        robo.turnLeft();
        robo.move();
        robo.turnLeft();
        robo.turnLeft();
        robo.turnLeft();
        robo.move();
        robo.putThing();
        robo.turnLeft();
        robo.turnLeft();
        robo.turnLeft();
    }
}
