package g30126.Andriesi.Claudiu.l3.e6;

public class MyPoint {
    private int x;
    private int y;

    //Constructor
    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    //Constructor
    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double distance(int x, int y) {
        return (Math.sqrt((this.x - x) * (this.x - x) + (this.y - y) * (this.y - y)));
    }

    public double distance(MyPoint x) {
        return (Math.sqrt((this.x - x.getX()) * (this.x - x.getX()) + (this.y - x.getY()) * (this.y - x.getY())));
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ')';
    }

}
