import org.junit.Test;

import static org.junit.Assert.assertEquals;

import g30126.Andriesi.Claudiu.l4.e4.Author;

public class TestAuthor {
    @Test
    public void testToString(){
        Author author = new Author("Ion Creanga", "ioncrenga@yahoo.com", 'M');
        assertEquals(author.getName() + "(" + author.getGender()+ ") at email " + author.getEmail(), author.toString()); }
}
