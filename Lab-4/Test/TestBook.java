import g30126.Andriesi.Claudiu.l4.e4.Author;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import g30126.Andriesi.Claudiu.l4.e5.Book;

public class TestBook {

    @Test
    public void testToString(){
        Book book = new Book("Amintiri din Copilarie", new Author("Ion Creanga", "ioncreanga@yahoo.com",'M'),10, 10);
        assertEquals(book.getName()+" by " + book.getAuthor().toString(), book.toString());
    }
}
