import g30126.Andriesi.Claudiu.l4.e4.Author;
import g30126.Andriesi.Claudiu.l4.e6.Book2;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestBook2 {
    @Test
    public void testToString(){
        Author[] a ={new Author("Ion Creanga",
                "ioncreanga@yahoo.com",'M'), new Author("Mihai Eminescu", "mihaiaminescu@yahoo.com",'M')};
        Book2 book2 = new Book2("Inginerie",a, 10,10);
        String res = "";
        res += book2.getName() + " by " + book2.getAuthors().length + " authors: ";
        for (Author b : book2.getAuthors())
            res += b.getName() + " ";
        assertEquals(res, book2.toString());
    }
}
