import g30126.Andriesi.Claudiu.l4.e3.Circle;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCircle {
    @Test
    public void testGetArea() {
        Circle circle = new Circle();
        assertEquals(Math.PI*circle.getRadius()*circle.getRadius(),circle.getArea(),0.01);
    }

    @Test
    public void testGetRadius(){
        Circle circle = new Circle();
        assertEquals(1.0, circle.getRadius(),0.01);
    }
}
