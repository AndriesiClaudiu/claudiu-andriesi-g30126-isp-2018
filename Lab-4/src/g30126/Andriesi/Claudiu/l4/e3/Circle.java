package g30126.Andriesi.Claudiu.l4.e3;

public class Circle {
    private double radius;
    private String color;

    //Constructor
    public Circle() {
        radius = 1.0;
        color = "red";
    }

    public Circle(double radius) {
        this();
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * getRadius() * getRadius();
    }
}
