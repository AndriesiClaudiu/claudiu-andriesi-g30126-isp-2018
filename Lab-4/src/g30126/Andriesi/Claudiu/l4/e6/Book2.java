package g30126.Andriesi.Claudiu.l4.e6;

import g30126.Andriesi.Claudiu.l4.e4.Author;


public class Book2 {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    //Constructor
    public Book2(String name, Author[] author, double price) {
        this.name = name;
        this.authors = author;
        this.price = price;
    }

    public Book2(String name, Author[] author, double price, int qtyInStock) {
        this.name = name;
        this.authors = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        String res = "";
        res += name + " by " + authors.length + " authors: ";
        for (Author a : authors)
            res += a.getName() + " ";
        return res;
    }

    public void printAuthors() {
        String aut = "";
        for (Author a : authors)
            System.out.println(a.getName());

    }

}

