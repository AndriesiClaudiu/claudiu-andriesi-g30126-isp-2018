package g30126.Andriesi.Claudiu.l4.e7;

import g30126.Andriesi.Claudiu.l4.e3.Circle;

public class Cyclindre extends Circle {
    private double height;

    //constructor
    Cyclindre() {
        super();
        height = 1;
    }

    Cyclindre(double radius) {
        super(radius);
    }

    Cyclindre(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea() * height;
    }

    @Override
    public double getArea(){
        return 2*getArea()+2*Math.PI*getRadius()*getHeight();
    }
}
