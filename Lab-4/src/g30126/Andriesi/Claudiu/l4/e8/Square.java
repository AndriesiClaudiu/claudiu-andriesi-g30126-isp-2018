package g30126.Andriesi.Claudiu.l4.e8;

public class Square extends Rectangle {

    //Constructor
    Square() {
        super();
    }

    Square(double side) {
        super(side, side);
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public double getSide() {
        return getLength();
    }

    public void setSide(double side) {
        setLength(side);
        setWidth(side);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString() {
        return "A Square with side " + getSide() +
                " , which is a subclass of " + super.toString();
    }
}
