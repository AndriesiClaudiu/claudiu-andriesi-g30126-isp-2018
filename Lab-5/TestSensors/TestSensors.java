import org.junit.Test;

import static org.junit.Assert.assertEquals;

import Andriesi.Claudiu.test3.*;

public class TestSensors {
    @Test
    public void testSensor() {
        Sensor sensor = new Sensor();
        assertEquals("Sensor value is " + sensor.getValue(), sensor.read());
    }

    @Test
    public void testHumiditySensor() {
        HumiditySensor sensor = new HumiditySensor();
        assertEquals("HumiditySensor value is " + sensor.getValue(), sensor.read());
    }

    @Test
    public void testTemperatureSensor() {
        TemperatureSensor sensor = new TemperatureSensor();
        assertEquals("TemperatureSensor value is " + sensor.getValue(), sensor.read());
    }

    @Test
    public void testProxSensor() {
        ProxSensor sensor = new ProxSensor();
        assertEquals("Sensor value is " + sensor.getValue(), sensor.read());
    }
}
