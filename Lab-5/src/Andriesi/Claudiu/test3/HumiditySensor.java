package Andriesi.Claudiu.test3;

public class HumiditySensor extends Sensor {
    @Override
    public String read() {
        return "HumiditySensor value is " + getValue();
    }
}
