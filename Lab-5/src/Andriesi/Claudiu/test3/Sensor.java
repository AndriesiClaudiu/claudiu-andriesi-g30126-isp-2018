package Andriesi.Claudiu.test3;

import java.util.Random;

public class Sensor {
    private int value;
    private String location;

    public Sensor(){
        value = 0;
        location = "None";
    }

    public String read() {
        return "Sensor value is " + value;
    }

    public int getValue() {
        return value;
    }

    public String getLocation() {
        return location;
    }

    public static void main(String[] args) {
        Sensor[] sensors = new Sensor[4];
        sensors[0] = new Sensor();
        sensors[1] = new TemperatureSensor();
        sensors[2] = new HumiditySensor();
        sensors[3] = new ProxSensor();

        for (Sensor x: sensors)
            System.out.println(x.read());

        int i = 0;
        Random random = new Random();
        while (i < 4) {
            int x = random.nextInt(4);
            switch (x) {
                case 0:
                    sensors[i] = new Sensor();
                    break;
                case 1:
                    sensors[i] = new TemperatureSensor();
                    break;
                case 2:
                    sensors[i] = new HumiditySensor();
                    break;
                case 3:
                    sensors[i] = new ProxSensor();
                    break;
            }
            i++;
        }
        for (i = 0;i < sensors.length;i++)
            System.out.println(sensors[i].read());
    }
}
