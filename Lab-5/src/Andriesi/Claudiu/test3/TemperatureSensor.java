package Andriesi.Claudiu.test3;

public class TemperatureSensor extends Sensor{
    @Override
    public String read(){
        return "TemperatureSensor value is " + getValue();
    }
}
