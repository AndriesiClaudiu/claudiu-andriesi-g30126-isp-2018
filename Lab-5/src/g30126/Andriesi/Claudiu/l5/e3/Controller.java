package g30126.Andriesi.Claudiu.l5.e3;

import java.awt.*;

public class Controller {

    private LightSensor lightSensor;
    private TemperatureSensor temperatureSensor;

    public Controller(LightSensor lightSensor, TemperatureSensor temperatureSensor) {
        this.lightSensor = lightSensor;
        this.temperatureSensor = temperatureSensor;
    }

    public void control() {
        int i = 0;
        while (i < 20) {
            System.out.println("Light Sensor Value: "+ lightSensor.readValue());
            System.out.println("Temperature Sensor Value " + temperatureSensor.readValue());
            try{
            Thread.sleep(1000);}
            catch (InterruptedException e){
                System.err.println(e);
            }
            i++;
        }
    }

    public static void main(String[] args) {
        Controller controller = new Controller(new LightSensor(), new TemperatureSensor());
        controller.control();
    }
}
