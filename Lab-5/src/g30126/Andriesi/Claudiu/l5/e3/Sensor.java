package g30126.Andriesi.Claudiu.l5.e3;

public abstract class Sensor {
    private String location;
    private int value;
    public abstract int readValue();

    public String getLocation() {
        return location;
    }

}
