package g30126.Andriesi.Claudiu.l6.e1;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    private ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(300,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public boolean addShape(Shape shape){
        if(!shapes.contains(shape)){
            shapes.add(shape);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){
        System.out.println(shapes.size() + " SHAPES");
        for(Shape shape : shapes){
            shape.draw(g);
            System.out.println("Draw " + shape.getId());
        }
    }

    public boolean deleteShape(String id){
        for(int i=0;i<shapes.size();i++){
            if(shapes.get(i).getId().equals(id)){
                shapes.remove(i);
                System.out.println("Shape with id:"+shapes.get(i).getId()+" was deleted");
                repaint();
                return true;
            }
        }
        System.out.println("The id does not exists!");

        return false;
    }
}
