package g30126.Andriesi.Claudiu.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) throws Exception{
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,"circle1",true,50);
        b1.addShape(s1);
        Thread.sleep(1000);
        Shape s2 = new Circle(Color.GREEN, 100,200,"circle2",false,50);
        b1.addShape(s2);
        Thread.sleep(1000);

        Shape s3 = new Rectangle(Color.BLUE,100, 100,"rectangle",false,50,100);
        b1.addShape(s3);
        Thread.sleep(1000);

        b1.deleteShape("");
        Thread.sleep(1000);

        b1.deleteShape("circle2");
        Thread.sleep(1000);

    }
}