package g30126.Andriesi.Claudiu.l6.e3;

import java.awt.*;

public class Circle implements Shape {

    private Color color;
    private int x;
    private int y;
    private final String id;
    private boolean filled;
    private int radius;

    public Circle(Color color, int x, int y, String id, boolean filled,int radius) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.filled = filled;
        this.radius = radius;
    }

    public Color getColor() {
        return color;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }


    public String getId() {
        return id;
    }

    public boolean isFilled() {
        return filled;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius);
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(isFilled()){
            g.fillOval(getX(),getY(),radius,radius);
        }
    }
}
