package g30126.Andriesi.Claudiu.l6.e3;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Rectangle rectangle = new Rectangle(Color.RED,100,100,"1",true,100,50);
        Circle circle = new Circle(Color.GREEN,100,100,"2",false,100);

        b1.addCircle(circle);
        b1.addRectangle(rectangle);
        b1.deleteCircle("2");
        b1.deleteCircle("0");
    }
}
