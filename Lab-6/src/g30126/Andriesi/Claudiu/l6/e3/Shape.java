package g30126.Andriesi.Claudiu.l6.e3;
import java.awt.*;

public interface Shape {
    void draw(Graphics g);
}