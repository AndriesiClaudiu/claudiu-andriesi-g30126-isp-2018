package g30126.Andriesi.Claudiu.l6.e4;
import java.util.ArrayList;

public class CharSeq implements CharSequence {

    private String name;

    public CharSeq(String name) {
        this.name = name;
    }

    @Override
    public int length() {
        if(this.name != null){
            return name.length();
        }
        return 0;
    }

    @Override
    public char charAt(int index) {
        if(this.name != null){
            return this.name.charAt(index-1);
        }
        return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if(this.name != null){
            return this.name.subSequence(start,end);
        }
        return null;
    }
}
