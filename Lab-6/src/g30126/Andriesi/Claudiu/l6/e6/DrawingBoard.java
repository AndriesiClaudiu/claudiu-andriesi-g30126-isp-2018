package g30126.Andriesi.Claudiu.l6.e6;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard extends JFrame {
    private ArrayList<Circle> circles = new ArrayList<>();

    public DrawingBoard(){
        this.setTitle("CircleFractal");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(500,500);
        this.setVisible(true);
    }

    public boolean addCircle(Circle circle){
        if(!circles.contains(circle)){
            circles.add(circle);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){
        for(int i=0;i<circles.size();i++){
            circles.get(i).draw(g);
        }
    }
}
