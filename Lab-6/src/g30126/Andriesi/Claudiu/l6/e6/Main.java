package g30126.Andriesi.Claudiu.l6.e6;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Circle circle = new Circle(100, Color.RED, 200, 200);

        b1.addCircle(circle);
//        while (circle.getRadius() >= 10) {
//            System.out.println("Radius="+circle.getRadius());
//            circle.setRadius(circle.getRadius() - 10);
//            b1.addCircle(new Circle(circle.getRadius(), Color.RED, circle.getX(), circle.getY()));
//        }

        while (circle.getRadius() >=10){
            circle.setRadius(circle.getRadius() - 10);
            b1.addCircle(new Circle(circle.getX() + circle.getRadius()/2,Color.RED,circle.getX(),circle.getY()));
            b1.addCircle(new Circle(circle.getX() - circle.getRadius()/2,Color.RED,circle.getX(),circle.getY()));
        }
    }
}
