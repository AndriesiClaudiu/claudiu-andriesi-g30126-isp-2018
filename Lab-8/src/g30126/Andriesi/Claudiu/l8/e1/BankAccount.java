package g30126.Andriesi.Claudiu.l8.e1;

import java.util.Comparator;
import java.util.Objects;

public class BankAccount implements Comparable<BankAccount>{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) {
        if (amount < 0) {
            if (amount - balance > 0)
                balance -= amount;
            else
                System.out.println("Nu sunt suficienti bani!");
        } else
            System.out.println("Suma incorecta!");
    }

    public void deposit(double amount) {
        if (amount < 0)
            System.out.println("Suma incorecta");
        else
            balance += amount;
    }

    @Override
    public boolean equals(Object o) {
        BankAccount ba = (BankAccount) o;
        if (owner.equals(ba.owner) && (balance == ba.balance))
            return true;
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public String toString() {
        return owner + " " + balance;
    }

    public int compareTo(BankAccount bankAccount){
        return (int)(balance - bankAccount.balance);
    }

    public int compare(BankAccount a, BankAccount b){
        return (int)(a.getBalance() - b.getBalance());
    }


}
