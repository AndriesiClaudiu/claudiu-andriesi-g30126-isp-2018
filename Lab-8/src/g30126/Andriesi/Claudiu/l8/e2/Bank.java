package g30126.Andriesi.Claudiu.l8.e2;

import g30126.Andriesi.Claudiu.l8.e1.BankAccount;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.prefs.BackingStoreException;

public class Bank {
    private ArrayList<BankAccount> accounts;

    Bank(ArrayList<BankAccount> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }


    public void printAccounts() {
        accounts.sort(new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount a, BankAccount b) {
                return (int) (a.getBalance() - b.getBalance());
            }
        });
        for (BankAccount x : accounts)
            System.out.println(x.toString());
    }

    public void printAccounts(int minBalance, int maxBalance) {
        for (BankAccount x : accounts)
            if (x.getBalance() >= minBalance && x.getBalance() < maxBalance)
                System.out.println(x.toString());
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount x : accounts) {
            if (x.getOwner().equals(owner))
                return x;
        }
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts() {
        return accounts;
    }
}
