package g30126.Andriesi.Claudiu.l8.e2;

import g30126.Andriesi.Claudiu.l8.e1.BankAccount;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.ArrayList;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ArrayList<BankAccount> list = new ArrayList<>();
        Bank bank = new Bank(list);
        bank.addAccount("Mihai", 100);
        bank.addAccount("Andrei", 1000);
        bank.addAccount("Mircea", 2000);
        bank.addAccount("Liviu", 10000);
        bank.printAccounts();
        System.out.println("-----------------------------");
        ArrayList<BankAccount> list2 = bank.getAllAccounts();
        list2.sort(new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return o1.getOwner().compareTo(o2.getOwner());
            }
        });
        for(BankAccount a :list2)
            System.out.println(a.toString());


    }
}
