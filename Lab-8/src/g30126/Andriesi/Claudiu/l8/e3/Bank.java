package g30126.Andriesi.Claudiu.l8.e3;

import g30126.Andriesi.Claudiu.l8.e1.BankAccount;


import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> accounts;

    Bank(TreeSet<BankAccount> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }


    public void printAccounts() {
        for (BankAccount x : accounts)
            System.out.println(x.toString());
    }

    public void printAccounts(int minBalance, int maxBalance) {
        for (BankAccount x : accounts)
            if (x.getBalance() >= minBalance && x.getBalance() < maxBalance)
                System.out.println(x.toString());
    }

    public BankAccount getAccount(String owner) {
        for (BankAccount x : accounts) {
            if (x.getOwner().equals(owner))
                return x;
        }
        return null;
    }

    public TreeSet<BankAccount> getAllAccounts() {
        return accounts;
    }
}
