package g30126.Andriesi.Claudiu.l8.e4;

import java.util.Collection;
import java.util.Objects;

public class Word {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
    @Override
    public boolean equals(Object a){
        Word w = (Word)a;
        if (w.getWord().equalsIgnoreCase(((Word) a).getWord()))
            return true;
        return false;
    }

    @Override
    public int hashCode(){
        int sum = 0;
        for (int i = 0 ; i <word.length(); i++){
            sum += word.charAt(i);
        }
        return sum;
    }
}
