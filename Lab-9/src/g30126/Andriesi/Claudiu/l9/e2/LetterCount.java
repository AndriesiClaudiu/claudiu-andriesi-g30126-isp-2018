package g30126.Andriesi.Claudiu.l9.e2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class LetterCount {
    public static void main(String[] args) {
        int count = 0;
        char myChar = 'v';
        File file = new File("F:\\IntelliJ\\Projects\\Lab-9\\src\\g30126\\Andriesi\\Claudiu\\l9\\e2\\data.txt");
        if (file.exists())
            try {
                FileInputStream inputStream = new FileInputStream(file);
                while (inputStream.available() > 0){
                    char caracter = (char)inputStream.read();
                    if (caracter == myChar)
                        count++;
                }
                System.out.println("Exista " + count + " caractere de " + myChar + ".");
            }catch (IOException ex){
                System.err.println(ex);
            }
        else
            System.out.println("File does not exist!");
    }
}
