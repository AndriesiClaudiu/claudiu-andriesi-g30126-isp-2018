package g30126.Andriesi.Claudiu.l9.e3;

import java.io.*;

public class EncDec {
    public static void main(String[] args) {
        if (args.length == 2) {
            File inFile = new File(args[1]);
            File outFile = null;
            if (!inFile.exists()) {
                System.out.println("Fisierul nu exista!");
                System.exit(0);
            }
            try {
                FileInputStream inputStream = new FileInputStream(inFile);
                FileOutputStream outputStream = null;
                if (args[0].equalsIgnoreCase("encript")) {
                    outFile = new File(args[1].substring(0, args[1].length() - 3) + ".dec");
                    outputStream = new FileOutputStream(outFile);
                    while (inputStream.available() > 0) {
                        outputStream.write((((char) inputStream.read()) << 1));
                    }

                } else if (args[0].equalsIgnoreCase("decript")) {
                    outFile = new File(args[1].substring(0, args[1].length() - 3) + ".enc");
                    outputStream = new FileOutputStream(outFile);
                    while (inputStream.available() > 0) {
                        outputStream.write((((char) inputStream.read()) >> 1));
                    }
                } else
                    System.out.println("Wrong option!");
                outputStream.close();
                inputStream.close();

            } catch (IOException e) {
                System.err.println(e);
            }
        }
        else
            System.out.println("Wrong number of arguments!");
    }
}

