package g30126.Andriesi.Claudiu.l9.e4;

import java.io.Serializable;

public class Car implements Serializable {
    private int model;
    private int price;

    public Car(int model, int price){
        this.model = model;
        this.price = price;
    }

    public int getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car_" +
                "model_" + model + "_price_" + price;
    }

}
