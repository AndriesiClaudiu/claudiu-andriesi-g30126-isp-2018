package g30126.Andriesi.Claudiu.l9.e4;

import java.io.*;
import java.util.ArrayList;

public class Serialize {
    public static void serialize(Car car){

        try{
            ObjectOutputStream ous =
                    new ObjectOutputStream(
                            new FileOutputStream(
                                    "F:\\IntelliJ\\Projects\\Lab-9\\src\\g30126\\Andriesi\\Claudiu\\l9\\e4" + car.toString() + ".ser"));
            ous.writeObject(car);
            ous.close();
        }
        catch (IOException e){
            System.err.println(e);
        }
    }

    public static Car deserialize(String nume){

        Car car=null;
        try{
            ObjectInputStream ois =
                    new ObjectInputStream(
                            new FileInputStream(
                                    "F:\\IntelliJ\\Projects\\Lab-9\\src\\g30126\\Andriesi\\Claudiu\\l9\\e4\\" + nume+ ".ser"));
            car =  (Car)ois.readObject();
            ois.close();
        }
        catch (IOException e){
            System.err.println(e);
        }
        catch (ClassNotFoundException e){
            System.err.println(e);
        }
        finally {
            return car;
        }
    }
}
