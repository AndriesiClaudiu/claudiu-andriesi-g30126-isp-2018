package Interface;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class ButtonPanel extends JPanel {

    private OperandButton add;
    private OperandButton substract;
    private OperandButton divide;
    private OperandButton multiply;
    private FunctionButton integrate;
    private FunctionButton clear;
    private FunctionButton reset;
    private FunctionButton derivate;
    //Constructor
    ButtonPanel() {
        super();
        setLayout(new FlowLayout(SwingConstants.CENTER));
        setPreferredSize(new Dimension(600, 150));
        add = new OperandButton("+");
        substract = new OperandButton("-");
        multiply = new OperandButton("*");
        divide = new OperandButton("/");
        integrate = new FunctionButton("Integrate");
        clear = new FunctionButton("Clear");
        reset = new FunctionButton("Reset");
        derivate = new FunctionButton("Derivate");
        add(add);
        add(substract);
        add(multiply);
        add(divide);
        add(integrate);
        add(clear);
        add(reset);
        add(derivate);
    }
}
