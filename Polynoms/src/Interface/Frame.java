package Interface;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    //Constructor
    Frame(String title, String imagePath) {
        super();
        setTitle(title);
        ImageIcon image = new ImageIcon(imagePath);
        setIconImage(image.getImage());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLocation(new Point(1100, 100));
        setMinimumSize(new Dimension(600, 500));
        getContentPane().setLayout(new GridLayout(2, 1, 50, 30));
    }
}
