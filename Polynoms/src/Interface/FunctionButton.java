package Interface;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FunctionButton extends JButton {

    //Constructor
    FunctionButton(String name) {
        super(name);
        setPreferredSize(new Dimension(140, 75));
        setHorizontalTextPosition(SwingConstants.CENTER);
        setBorder(null);
        setForeground(Color.gray);
        setBackground(Color.decode("#FBFBFB"));
        setFont(new Font("Baron", Font.BOLD, 30));


        super.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setBackground(Color.decode("#BDBEBD"));
            }

            @Override
            public void mousePressed(MouseEvent e) {
                setBackground(Color.decode("#BDBEBD"));
                setBorder(new LineBorder(Color.decode("#EBECEB"), 3, false));

            }

            @Override
            public void mouseReleased(MouseEvent e) {
                setBackground(Color.decode("#E2E2E2"));
                setBorder(new LineBorder(Color.decode("#EEEEEE"), 3, false));
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                setBackground(Color.decode("#E2E2E2"));
                setBorder(null);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                setBackground(Color.decode("#FBFBFB"));
                setBorder(null);

            }
        });

    }
}
