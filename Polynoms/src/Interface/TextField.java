package Interface;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.*;

public class TextField extends JTextField {


    //Constructor
    TextField() {
        super();
        setPreferredSize(new Dimension(560, 60));
        setBackground(Color.decode("#EBECEB"));
        setFont(new Font("Baron", Font.BOLD, 30));
        setHorizontalAlignment(JTextField.CENTER);
        setBorder(new LineBorder(Color.gray, 3, true));
    }
}
