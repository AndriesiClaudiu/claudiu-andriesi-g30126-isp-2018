package Interface;

import javax.swing.*;
import java.awt.*;

public class TextPanel extends JPanel {

    private TextField firstField;
    private TextField secondField;
    private TextField thirdField;

    //Constructor
    TextPanel() {
        super();
        setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
        setPreferredSize(new Dimension(600, 300));
        firstField = new TextField();
        secondField = new TextField();
        thirdField = new TextField();
        add(firstField);
        add(secondField);
        add(thirdField);
    }
}
