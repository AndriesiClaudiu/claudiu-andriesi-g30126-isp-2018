package Interface;

import javax.jws.soap.SOAPBinding;

public class UserInterface extends Frame{


    //Contructor
    UserInterface() {
        super("Calculator", "calculator.png");
        add(new TextPanel());
        add(new ButtonPanel());
        setVisible(true);
    }
}
