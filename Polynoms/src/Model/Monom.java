package Model;

import java.lang.Number;

public class Monom<C extends Number, E extends Number> {
    private C coeficient;
    private E exponent;

    //Constructor
    Monom(C coeficient, E exponent){
        this.coeficient = coeficient;
        this.exponent = exponent;
    }

    //Set coeficient method
    public void setCoeficient(C coeficient){
        this.coeficient = coeficient;
    }

    //Set exponent method
    public void setExponent(E exponent){
        this.exponent = exponent;
    }

    //Get coeficient
    public C getCoeficient(){
        return coeficient;
    }

    //Get exponent
    public E getExponent(){
        return exponent;
    }

    //ToString
    public String toString() {
        return (coeficient + "X^" + exponent + " ");
    }
}
