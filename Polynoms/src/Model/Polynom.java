package Model;

import java.util.List;

public class Polynom {

    private List<Monom> list;

    //Constructor
    public Polynom(){
        this.list = null;
    }

    //Constructor
    public Polynom(List<Monom> list){
        this.list = list;
    }

    //Build polynom by adding new monoms
    public void buildPolynom(Monom x){
        list.add(x);
    }

    //Getter polynom list
    public List<Monom> getPolynom(){
        return list;
    }
    public String toString(){
        String polynom = "";
        for (Monom x: list){
            polynom += x.toString();
        }
        return polynom;
    }
}
