package Service;

import Model.Polynom;
import Model.Monom;

public class Calculator {

    /**
     *
     * @return sum of polynoms a and b
     */
    static public Polynom add(Polynom a, Polynom b){
        Polynom result = new Polynom();
        for (Monom x: a.getPolynom()){
            result.buildPolynom(x);
        }
        for (Monom x: b.getPolynom()){
            result.buildPolynom(x);
        }
        return formatResult(result);
    }

    /**
     *
     * @return difference between polynoms a and b
     */
    static public Polynom substract(Polynom a, Polynom b){
        Polynom result = new Polynom();
        for (Monom x: a.getPolynom()){
            result.buildPolynom(x);
        }
        for (Monom x: b.getPolynom()){
            Monom minusX = new Monom(-x.getCoeficient(), x.getExponent());
            result.buildPolynom(minusX);
        }
        return formatResult(result);
    }

    /**
     *
     * @return division of a to b
     */
    static public Polynom divide(Polynom a, Polynom b){
        Polynom result = new Polynom();
        return result;
    }

    /**
     *
     * @return product between a and b
     */
    static public Polynom multiply(Polynom a,Polynom b){
        Polynom result = new Polynom();
        for(Monom x: a.getPolynom()){
            for(Monom y: b.getPolynom()){
                result.buildPolynom(MonomCalculator.multiply(x, y));
            }
        }
        return formatResult(result);
    }

    /**
     *
     * @return polynom a integrated
     */
    static public Polynom integrate(Polynom a){
        Polynom result = new Polynom();
        for (Monom x: a.getPolynom()){
            result.buildPolynom(MonomCalculator.integrate(x));
        }
        return result;
    }

    /**
     *
     * @return polynom a derivated
     */
    static public Polynom derivate(Polynom a){
        Polynom result = new Polynom();
        for (Monom x: a.getPolynom()){
            result.buildPolynom(MonomCalculator.derivate(x));
        }
        return result;
    }

    /**
     *
     * @param result
     * @return a polynom i which every exponent appears once
     */
    private static Polynom formatResult(Polynom result){
        return result;
    }
}


class MonomCalculator{

    /**
     *
     * @return sum of monoms a and b
     */
    static public Monom add(Monom a, Monom b){
        if (a.getExponent() == b.getExponent())
            return new Monom(a.getCoeficient() + b.getCoeficient(), a.getExponent());

    }

    /**
     *
     * @return difference of monoms a and b
     */
    static public Monom substract(Monom a, Monom b){
        if (a.getExponent() == b.getExponent())
            return new Monom(a.getCoeficient() - b.getCoeficient(), a.getExponent());
    }

    /**
     *
     * @return division between a and b
     */
    static public Monom divide(Monom a, Monom b){
        return new Monom(a.getCoeficient() / b.getCoeficient(), a.getExponent() - a.getExponent());
    }

    /**
     *
     * @return product between monoms a and b
     */
    static public Monom multiply(Monom a, Monom b){
        return new Monom(a.getCoeficient() * b.getCoeficient(), a.getExponent() + a.getExponent());
    }

    /**
     *
     * @return monom b derivated
     */
    static public Monom integrate(Monom a){
        return new Monom(a.getCoeficient()/a.getExponent(), a.getExponent() + 1);
    }

    /**
     *
     * @return monom a derivated
     */
    static public Monom derivate(Monom a){
        return new Monom(a.getExponent()*a.getCoeficient(), a.getExponent() - 1);
    }
}
